# TCC - Tonto's Code Compiler

This is a code compiler to compile textual ontology representations into working code,
providing a database with the ontology structure and a web API to interact with the DB.

Although the name suggest as such, this project doesn't actually use Tonto as a source language.
It was a initial intention to, but it was decided to use the more generic Ontouml-js representation (which Tonto can compile to)

## Supported Source Languages

* ontouml-js

## Supported Target Languages/Frameworks

* Java
  * Micronaut
