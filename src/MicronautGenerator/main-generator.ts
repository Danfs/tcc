import fs from "fs";
import type { Package } from "../generator-types";

import { generateConfigs } from "./config-generator";
import { generateModules } from "./module-generator";

export function generate(packages: Readonly<Package>, target_folder: string) : void {
    fs.mkdirSync(target_folder, {recursive:true})
    generateModules(packages, target_folder)
    generateConfigs(target_folder)
}