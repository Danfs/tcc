import path from "path";
import fs from 'fs'

/**
 * Capitaliza uma string
 * 
 * @param str - String a ser capitalizada
 * @returns A string capitalizada
 */
export function capitalizeString(str: string) : string {
    return str.charAt(0).toUpperCase() + str.slice(1)
}

/**
 * Aplica `path.join` nos argumentos passados, e cria o caminho gerado caso não exista
 * 
 * @param args - Caminho para ser construído
 * @returns O caminho construído e normalizado, o mesmo retorno que `path.join(args)`
 */
export function createPath(...args: readonly string[]) : string {
    const PATH = path.join(...args)
    if(!fs.existsSync(PATH)) {
        fs.mkdirSync(PATH, { recursive: true })
    }
    return PATH
}

/**
 * Pseudo map, aplicando funções em cima de sets ao invés de arrays
 * 
 * @param set Conjundo de elementos do tipo T
 * @param fn Função que recebe um argumento do tipo T
 * @returns Array contendo os retornos das aplicações de `fn` nos elementos de `set`
 */
export function setMap<T, U>(set: Set<T>, fn: (elem: T) => U) : U[] {
    const ret: U[] = []
    set.forEach(x => {
        ret.push(fn(x))
    })
    return ret
}
