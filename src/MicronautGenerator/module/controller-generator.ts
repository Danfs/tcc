import { expandToString, expandToStringWithNL, Generated } from "langium";
import type { Entity, Relation, Role } from "../../generator-types";
import { role_attributes } from "./helpers";
import { capitalizeString } from "../generator-utils";

export function generateController(entity: Readonly<Entity>) : Generated {
    return expandToStringWithNL`
        package ${entity.package}.controllers;

        import ${entity.package}.models.${entity.name};
        import ${entity.package}.dtos.${entity.name}InputDto;
        import ${entity.package}.dtos.${entity.name}OutputDto;
        import ${entity.package}.applications.${entity.name}Application;

        import java.net.URI;
        import java.util.UUID;
        import java.util.List;
        import jakarta.inject.Inject;
        import javax.validation.Valid;
        import javax.transaction.Transactional;
        import lombok.RequiredArgsConstructor;
        import io.micronaut.http.HttpResponse;
        import io.micronaut.http.MediaType;
        import io.micronaut.http.annotation.Body;
        import io.micronaut.http.annotation.Controller;
        import io.micronaut.http.annotation.Delete;
        import io.micronaut.http.annotation.Get;
        import io.micronaut.http.annotation.Post;
        import io.micronaut.http.annotation.Put;
        import io.micronaut.http.annotation.PathVariable;
        import io.micronaut.validation.Validated;

        @Controller("/${entity.name.toLowerCase()}s")
        @RequiredArgsConstructor(onConstructor_ = {@Inject})
        @Validated
        public class ${entity.name}Controller {
            private final ${entity.name}Application ${entity.name.toLowerCase()}App;

            @Post(uri = "/", consumes = MediaType.APPLICATION_JSON)
            @Transactional
            public HttpResponse<Void> create(@Body @Valid ${entity.name}InputDto dto) {
                var saved = this.${entity.name.toLowerCase()}App.save(dto);
                return HttpResponse.created(URI.create("/${entity.name.toLowerCase()}s/" + saved.getId()));
            }

            @Get(uri = "/", produces = MediaType.APPLICATION_JSON)
            @Transactional
            public HttpResponse<List<${entity.name}OutputDto>> getAll() {
                var body = this.${entity.name.toLowerCase()}App.findAll()
                    .stream()
                    .map(elem -> this.modelToOutputDto(elem))
                    .toList();
                return HttpResponse.ok(body);
            }

            @Get(uri = "/{id}", produces = MediaType.APPLICATION_JSON)
            @Transactional
            public HttpResponse<${entity.name}OutputDto> getById(@PathVariable UUID id) {
                var elem = this.${entity.name.toLowerCase()}App.findById(id);
                return HttpResponse.ok(this.modelToOutputDto(elem));
            }

            @Put(uri = "/{id}", consumes = MediaType.APPLICATION_JSON)
            @Transactional
            public HttpResponse<?> updateById(@PathVariable UUID id, @Body @Valid ${entity.name}InputDto dto) {
                var elem = this.${entity.name.toLowerCase()}App.update(id, dto);
                return HttpResponse.ok(this.modelToOutputDto(elem));
            }

            @Delete(uri = "/{id}", produces = MediaType.APPLICATION_JSON)
            @Transactional
            public HttpResponse<?> deleteById(@PathVariable UUID id) {
                this.${entity.name.toLowerCase()}App.delete(id);
                return HttpResponse.noContent();
            }

            ${generateToOutputDTO(entity)}
        }
    `
}

function generateToOutputDTO(entity: Readonly<Entity>) : Generated {
    const relationToOutputField = ({name, cardinality}: Readonly<Relation>) => {
        return cardinality === "OneToOne" || cardinality === "ManyToOne"
            ? `elem.get${capitalizeString(name.toLowerCase())}() == null ? null : elem.get${capitalizeString(name.toLowerCase())}().getId(),`
            : `elem.get${capitalizeString(name.toLowerCase())}().stream().map(x -> x.getId()).toList(),`
    }
    const roleToOutputFields = (role : Readonly<Role>) => {
        const attrs = role_attributes(role).map(a => `elem.get${capitalizeString(a.name)}(),`)
        const rels = role.relations.map(relationToOutputField)

        return attrs.concat(rels)
    }

    const entity_name = entity.name
    return expandToString`
        private ${entity_name}OutputDto modelToOutputDto(${entity_name} elem) {
            return new ${entity_name}OutputDto(
                elem.getId(),
                ${entity.extended_attributes.map(a => `elem.get${capitalizeString(a.name)}(),`).join('\n')}
                ${entity.phases.length > 0 ? `elem.getPhase().toString(),` : undefined}
                ${entity.extended_relations.map(relationToOutputField).join('\n')}
                ${entity.subroles.flatMap(roleToOutputFields).join('\n')}
                elem.getCreatedAt()
            );
        }
    `
}