import { expandToString, expandToStringWithNL, Generated, toString } from "langium";
import type { Attribute, Entity, Relation, Role } from "../../generator-types";
import { capitalizeString } from "../generator-utils";
import { role_attributes } from "./helpers";

function processRequiredAttributes(attrs: readonly Attribute[]) : Generated[] {
    return attrs.map(a => expandToString`
        @NotNull ${capitalizeString(a.type)} ${a.name}
    `)
}

function processAttributes(attrs: readonly Attribute[]) : Generated[] {
    return attrs.map(a => `${capitalizeString(a.type)} ${a.name}`)
}

function processRelations(relations: readonly Relation[], input: boolean) : Generated[] {
    return relations.map(r => {
        return (r.cardinality === "ManyToMany" || r.cardinality === "OneToMany")
            ? `@NotNull${input ? ` @Size(min=${r.minimum})` : ''} List<UUID> ${r.name.toLowerCase()}_ids`
            : `UUID ${r.name.toLowerCase()}_id`
        }
    )
}

function processRole(role: Role, input: boolean) : Generated[] {
    const attrs = processAttributes(role_attributes(role))
    const rels = input
        ? processRelations(getInputRelations(role.relations),  input)
        : processRelations(getOutputRelations(role.relations), input)

    return attrs.concat(rels)
}

export function generateInputDTO(entity: Readonly<Entity>) : Generated {
    const attrs_str = processRequiredAttributes(entity.extended_attributes)
    const rels_str = processRelations(getInputRelations(entity.extended_relations), true)
    const roles_str = entity.subroles.flatMap(r => processRole(r, true))

    return expandToStringWithNL`
        package ${entity.package}.dtos;

        import java.util.UUID;
        import java.util.List;
        import javax.validation.constraints.Size;
        import javax.validation.constraints.NotNull;
        import io.micronaut.core.annotation.Introspected;

        @Introspected
        public record ${entity.name}InputDto(
            ${attrs_str
                .concat(entity.phases.length > 0 ? ['@NotNull String phase'] : [])
                .concat(rels_str.map(toString))
                .concat(roles_str)
                .join(',\n')
            }
        ) {}
    `
}

export function generateOutputDTO(entity: Readonly<Entity>) : Generated {
    const attrs_str = processRequiredAttributes(entity.extended_attributes)
    const rels_str = processRelations(getOutputRelations(entity.extended_relations), false)
    const roles_str = entity.subroles.flatMap(r => processRole(r, false))

    return expandToStringWithNL`
        package ${entity.package}.dtos;

        import java.util.UUID;
        import java.util.List;
        import java.time.LocalDateTime;
        import javax.validation.constraints.NotNull;
        import javax.validation.constraints.NotBlank;

        public record ${entity.name}OutputDto(
            @NotBlank UUID id,
            ${attrs_str
                .concat(entity.phases.length > 0 ? ['@NotNull String phase'] : [])
                .concat(rels_str.map(toString))
                .concat(roles_str)
                .map(str => toString(str)+',\n')
                .join('')
            }LocalDateTime createdAt
        ) {}
    `
}

/**
 * Dada uma lista de Relations, retorna a lista de quais dessas relações são passadas no InputDTO.
 */
export function getInputRelations(relations: readonly Relation[]) : Relation[] {
    return relations.filter(r => r.owner)
}

/**
 * Dada uma lista de Relation, retorna a lista de quais dessas relações são passadas no OutputDTO.
 * 
 * No momento, isso se resume a "qualquer relação exceto OneToOne que você não é dono", pois pode ser um null pointer
 */
export function getOutputRelations(relations: readonly Relation[]) : readonly Relation[] {
    return relations
}
