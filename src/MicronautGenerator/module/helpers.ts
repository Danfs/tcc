import type { Attribute, Entity, Relation, Role } from "../../generator-types";
import { isEntity } from "../../generator-types";

/**
 * Given a Role, return an array with an identity attribute (boolean is_{rolename})
 * and the regular attributes proccessed to the format {role_name}_{attr_name}
 * @param param0 the role to extract the attributes from
 * @returns 
 */
export function role_attributes({name, attributes}: Role) : Attribute[] {
    return [{type: 'boolean', name: `is_${name.toLowerCase()}`}].concat(attributes.map(a => {
        return {type: a.type, name: `${name.toLowerCase()}_${a.name.toLowerCase()}`}
    }))
}

/**
 * Given a Relation, return the Entity that it targets.
 * The annotation claims that `undefined` could be returned, but it shouldn't happen after the reading is complete
 */
export function get_relation_target_entity({target}: Relation) : Entity | undefined {
    return isEntity(target)
        ? target
        : target.role_of
}
