import { expandToStringWithNL, Generated } from "langium"
import type { Entity, Role } from "../../generator-types"
import { role_attributes } from "./helpers"
import { getInputRelations } from "./dtos-generator"

export function generateNotFoundException(package_name: string) : Generated {
    return expandToStringWithNL`
        package ${package_name}.exceptions;

        public class BaseException extends RuntimeException {
            public BaseException(String str) {
                super(str);
            }
        }
    `
}

export function generateBaseHandler(package_name: string) : Generated {
    return expandToStringWithNL`
        package ${package_name}.exceptions;

        import jakarta.inject.Singleton;
        import lombok.RequiredArgsConstructor;
        import io.micronaut.context.annotation.Requires;
        import io.micronaut.http.HttpRequest;
        import io.micronaut.http.HttpResponse;
        import io.micronaut.http.annotation.Produces;
        import io.micronaut.http.server.exceptions.ExceptionHandler;
        import io.micronaut.http.server.exceptions.response.ErrorContext;
        import io.micronaut.http.server.exceptions.response.ErrorResponseProcessor;

        @Produces
        @Singleton
        @Requires(classes = { BaseException.class })
        @RequiredArgsConstructor
        public class BaseHandler implements ExceptionHandler<BaseException, HttpResponse<?>> {
            private final ErrorResponseProcessor<?> errorResponseProcessor;

            @Override
            public HttpResponse<?> handle(HttpRequest request, BaseException exception) {
                return errorResponseProcessor.processResponse(
                    ErrorContext.builder(request)
                        .cause(exception)
                        .errorMessage(exception.getMessage())
                        .build(),
                    HttpResponse.notFound()
                );
            }
        }
    `
}

export function generateClassNotFoundException(entity: Readonly<Entity>) : Generated {
    return expandToStringWithNL`
        package ${entity.package}.exceptions;

        import java.util.UUID;

        public class ${entity.name}NotFoundException extends BaseException {
            public ${entity.name}NotFoundException(UUID id) {
                super("${entity.name} [id = "+id+"] was not found");
            }
        }
    `
}

export function generatePhaseNotFoundException(entity: Readonly<Entity>) : Generated {
    return expandToStringWithNL`
        package ${entity.package}.exceptions;

        public class ${entity.name}PhaseNotFoundException extends BaseException {
            public ${entity.name}PhaseNotFoundException(String phase) {
                super("Phase of ${entity.name} '"+phase+"' was not found. Valid options are: ${entity.phases.join(' | ')}");
            }
        }
    `
}

export function generateMissingRoleValueException(entity: Readonly<Entity>, role: Readonly<Role>) : Generated {
    const role_attrs = role_attributes(role)
    return expandToStringWithNL`
        package ${entity.package}.exceptions;

        public class ${entity.name}${role.name}MissingValueException extends BaseException {
            public ${entity.name}${role.name}MissingValueException() {
                super("Value required for the Role ${role.name} missing, but is_${role.name.toLowerCase()} is true. Values required for this Role are: ${role_attrs.map(a => a.name.toLowerCase()).concat(getInputRelations(role.relations).map(r => r.name.toLowerCase())).join(', ')}.");
            }
        }
    `
}

export function generateNoRoleRelationException(entity: Readonly<Entity>) : Generated {
    return expandToStringWithNL`
        package ${entity.package}.exceptions;

        import java.util.UUID;

        public class RelationMissingRoleException extends BaseException {
            public RelationMissingRoleException(UUID id, String role_name) {
                super("Entity [id = "+id+"] is not of role "+role_name+".");
            }
        }
    `
}

export function generateInsufficientCardinalityException(entity: Entity) : Generated {
    return expandToStringWithNL`
        package ${entity.package}.exceptions;

        public class InsufficientCardinalityException extends BaseException {
            public InsufficientCardinalityException(String relation_name, int minimum, int actual) {
                super("Insufficient relations passed for relation "+relation_name+". Minimum: "+minimum+" - Got: "+actual+".");
            }
        }
    `
}