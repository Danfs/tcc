import { Generated, expandToStringWithNL } from "langium"
import { isEntity } from "../../generator-types"
import type { Attribute, Entity, Relation, Role } from "../../generator-types"
import { get_relation_target_entity, role_attributes } from "./helpers"
import { getInputRelations } from "./dtos-generator"
import { capitalizeString } from "../generator-utils"

export function generateApplication(entity: Readonly<Entity>) : Generated {
    const entity_input_relations = getInputRelations(entity.extended_relations)
    const all_input_relations = entity_input_relations.concat(
        entity.subroles.flatMap(r => getInputRelations(r.relations))
    )
    const applications_used = Array.from(new Set(all_input_relations.map(get_relation_target_entity)))

    const has_phases = entity.phases.length > 0
    const has_many_to_many = all_input_relations.find(r => r.cardinality === "ManyToMany") !== undefined

    return expandToStringWithNL`
        package ${entity.package}.applications;

        import ${entity.package}.models.${entity.name};
        import ${entity.package}.dtos.${entity.name}InputDto;
        import ${entity.package}.repositories.${entity.name}Repository;
        import ${entity.package}.exceptions.${entity.name}NotFoundException;
        import ${entity.package}.exceptions.RelationMissingRoleException;
        ${has_many_to_many ? `import ${entity.package}.exceptions.InsufficientCardinalityException;` : undefined}
        ${has_phases ? `import ${entity.package}.exceptions.${entity.name}PhaseNotFoundException;` : undefined}
        ${entity.subroles.length > 0
            ? entity.subroles.map(r => `import ${entity.package}.exceptions.${entity.name}${r.name}MissingValueException;`).join('\n')
            : undefined
        }

        ${applications_used.filter(e => e && e.package != entity.package).map(e => `import ${e?.package}.applications.${e?.name}Application;`).join('\n')}
        import java.util.UUID;
        import java.util.List;
        ${has_many_to_many ? `import java.util.HashSet;` : undefined}
        import jakarta.inject.Singleton;
        import lombok.RequiredArgsConstructor;

        @Singleton
        @RequiredArgsConstructor
        public class ${entity.name}Application {
            private final ${entity.name}Repository repo;
            ${applications_used.map(e => `private final ${e?.name}Application ${e?.name.toLowerCase()}App;`).join('\n')}

            public ${entity.name} save(${entity.name}InputDto dto) {
                ${entity_input_relations.length > 0
                    ? entity_input_relations.map(generateRelations).join('\n')
                    : undefined
                }
                ${has_phases ? `var phase = ${entity.name}.Phase.get(dto.phase()).orElseThrow(() -> new ${entity.name}PhaseNotFoundException(dto.phase()));\n` : undefined}
                var builder = ${entity.name}.builder()
                    ${entity_input_relations.map(r => {
                        const name = r.name.toLowerCase()
                        return r.cardinality === "ManyToMany"
                            ? `.${name}(new HashSet<>(${name}))`
                            : `.${name}(${name})`
                    }).join('\n')}
                    ${has_phases ? `.phase(phase)` : undefined}
                    ${entity.extended_attributes.map(a => `.${a.name.toLowerCase()}(dto.${a.name.toLowerCase()}())`).join('\n')};

                ${entity.subroles.map(r => generatePostRoleConditional(r))}
                var data = builder.build();
                return this.repo.save(data);
            }

            public List<${entity.name}> findAll() {
                return this.repo.findAll();
            }

            public ${entity.name} findById(UUID id) {
                return this.repo.findById(id).orElseThrow(() -> new ${entity.name}NotFoundException(id));
            }

            public ${entity.name} update(UUID id, ${entity.name}InputDto dto) {
                ${entity_input_relations.length > 0
                    ? entity_input_relations.map(generateRelations).join('\n')
                    : undefined
                }
                ${has_phases ? `var phase = ${entity.name}.Phase.get(dto.phase()).orElseThrow(() -> new ${entity.name}PhaseNotFoundException(dto.phase()));\n` : undefined}
                var elem = this.repo.findById(id).orElseThrow(() -> new ${entity.name}NotFoundException(id));
                ${entity_input_relations.map(r => {
                    const name = r.name.toLowerCase()
                    return r.cardinality === "ManyToMany"
                        ? `elem.set${capitalizeString(name)}(new HashSet<>(${name}));`
                        : `elem.set${capitalizeString(name)}(${name});`
                }).join('\n')}
                ${entity.extended_attributes.map(a => `elem.set${capitalizeString(a.name)}(dto.${a.name.toLowerCase()}());`).join('\n')}
                ${has_phases ? `elem.setPhase(phase);` : undefined}

                ${entity.subroles.map(r => generatePutRoleConditional(r))}
                return this.repo.update(elem);
            }

            public void delete(UUID id) {
                var elem = this.repo.findById(id).orElseThrow(() -> new ${entity.name}NotFoundException(id));
                this.repo.delete(elem);
            }
        }
    `
}

function generateRelations(relation: Relation) : Generated {
    const name = relation.name.toLowerCase()
    const target_entity = isEntity(relation.target)
        ? relation.target
        : relation.target.role_of

    if(relation.cardinality === "ManyToMany") {
        return expandToStringWithNL`
            ${relation.minimum < Infinity
                ? `if(dto.${name}_ids().size() < ${relation.minimum}) {throw new InsufficientCardinalityException("${name}_ids", ${relation.minimum}, dto.${name}_ids().size());}`
                : undefined
            }
            var ${name} = dto.${name}_ids().stream().map(id_ -> ${target_entity?.name.toLowerCase()}App.findById(id_)).toList();
            ${!isEntity(relation.target)
                ? expandToStringWithNL`
                    for(var elem : ${name}) {
                        if(elem.getIs_${relation.target.name.toLowerCase()}() == false) {
                            throw new RelationMissingRoleException(elem.getId(), "${relation.target.name}");
                        }
                    }
                `
                : undefined
            }
        `
    } else {
        if(relation.minimum == 0) {
            return expandToStringWithNL`
                // Optional relation
                var ${name} = (dto.${name}_id() == null)
                    ? null
                    : ${target_entity?.name.toLowerCase()}App.findById(dto.${name}_id());
                ${!isEntity(relation.target)
                    ? `if(${name} != null && ${name}.getIs_${relation.target.name}() == false) {
                        throw new RelationMissingRoleException(dto.${name}_id(), "${relation.target.name}")
                    }`
                    : undefined
                }
            `
        } else {
            return expandToStringWithNL`
                var ${name} = ${target_entity?.name.toLowerCase()}App.findById(dto.${name}_id());
                ${!isEntity(relation.target)
                    ? `if(${name}.getIs_${relation.target.name}() == false) {
                        throw new RelationMissingRoleException(dto.${name}_id(), "${relation.target.name}")
                    }`
                    : undefined
                }
            `
        }
    }
}

function generatePostRoleConditional(role: Role) : Generated {
    const [is_role, ...role_attrs] = role_attributes(role)
    const is_role_name = is_role.name.toLowerCase()
    const relations = getInputRelations(role.relations)

    return expandToStringWithNL`
        boolean ${is_role_name} = Boolean.TRUE.equals(dto.${is_role_name}());
        builder = builder.${is_role_name}(${is_role_name});
        if(${is_role_name}) {
            ${generateRoleValuesCondition(role, role_attrs)}
            ${relations.map(generateRelations).join('\n')}
            builder = builder
                ${role_attrs.map(a => `.${a.name.toLowerCase()}(dto.${a.name.toLowerCase()}())`).join('\n')}
                ${relations.map(r => {
                    const name = r.name.toLowerCase()
                    return r.cardinality === "ManyToMany"
                        ? `.${name}(new HashSet<>(${name}))`
                        : `.${name}(${name})`
                }).join('\n')};
        }
    `
}

function generatePutRoleConditional(role: Role) : Generated {
    const [is_role, ...role_attrs] = role_attributes(role)
    const is_role_name = is_role.name.toLowerCase()
    const relations = getInputRelations(role.relations)

    return expandToStringWithNL`
        boolean ${is_role_name} = Boolean.TRUE.equals(dto.${is_role_name}());
        elem.set${capitalizeString(is_role.name)}(${is_role_name});
        if(${is_role_name}) {
            ${generateRoleValuesCondition(role, role_attrs)}
            ${relations.map(generateRelations).join('\n')}
            ${role_attrs.map(a => `elem.set${capitalizeString(a.name)}(dto.${a.name.toLowerCase()}())`).join('\n')};
            ${relations.map(r => {
                const name = r.name.toLowerCase()
                return r.cardinality === "ManyToMany"
                    ? `elem.set${capitalizeString(name)}(new HashSet<>(${name}));`
                    : `elem.set${capitalizeString(name)}(${name});`
            }).join('\n')}
        } else {
            ${role_attrs.map(a => `elem.set${capitalizeString(a.name)}(null)`).join('\n')};
            ${relations.map(r => {
                const name = r.name.toLowerCase()
                return r.cardinality === "ManyToMany"
                    ? `elem.set${capitalizeString(name)}(new HashSet<>());`
                    : `elem.set${capitalizeString(name)}(null);`
            }).join('\n')}
        }
    `
}

function generateRoleValuesCondition(role: Role, role_attrs: Attribute[]) : Generated {
    return expandToStringWithNL`
        if(${
            role_attrs.map(a => `dto.${a.name.toLowerCase()}() == null`)
            .concat(getInputRelations(role.relations).map(r => `dto.${r.name.toLowerCase()}_id() == null`))
            .join(' || ')
        }) {
            throw new ${role.role_of?.name}${role.name}MissingValueException();
        }
    `
}