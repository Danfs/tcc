import { CompositeGeneratorNode, expandToStringWithNL, Generated } from "langium";
import type { Entity, Relation, Role } from "../../generator-types";
import { isEntity } from "../../generator-types";
import { capitalizeString, setMap } from "../generator-utils";
import { role_attributes } from "./helpers";

export function generateModel(entity: Readonly<Entity>) : Generated {
    const has_phase = entity.phases.length > 0

    const relations_used = new Set(
        entity.relations
        .concat(entity.subroles.flatMap(r => r.relations))
        .map(r => r.cardinality)
    )

    return expandToStringWithNL`
        package ${entity.package}.models;

        import lombok.Getter;
        import lombok.Setter;
        import lombok.Builder;
        import lombok.NoArgsConstructor;
        import lombok.AllArgsConstructor;
        import lombok.experimental.SuperBuilder;

        import javax.persistence.Id;
        ${entity.is_supertype
            ? `import javax.persistence.Inheritance;\nimport javax.persistence.InheritanceType;`
            : undefined
        }
        import javax.persistence.Table;
        import javax.persistence.Entity;
        ${relations_used.size > 0 
            ? setMap(relations_used, card => `import javax.persistence.${card};`).join('\n')
            : undefined
        }
        import javax.persistence.JoinColumn;
        ${relations_used.has("ManyToMany")
            ? `import javax.persistence.JoinTable;`
            : undefined
        }
        import javax.persistence.CascadeType;
        import javax.persistence.GeneratedValue;

        import org.hibernate.annotations.GenericGenerator;

        import java.util.Set;
        import java.util.UUID;
        import java.util.HashSet;
        import java.util.Objects;
        ${has_phase ? `import java.util.Optional;` : undefined}
        ${!entity.extends ? `import java.io.Serializable;` : undefined}
        import java.time.LocalDateTime;

        @Getter
        @Setter
        @Entity
        @SuperBuilder
        @NoArgsConstructor
        @AllArgsConstructor
        @Table(name = "${entity.name.toLowerCase()}")
        ${entity.is_supertype ? '@Inheritance(strategy = InheritanceType.JOINED)' : undefined}
        public class ${entity.name} ${entity.extends ? `extends ${entity.extends.name}` : 'implements Serializable'} {
            ${has_phase ? generatePhases(entity) : undefined}
            @Id
            @GeneratedValue(generator = "uuid")
            @GenericGenerator(name = "uuid", strategy = "uuid2")
            UUID id;

            ${entity.attributes.map(a => `${capitalizeString(a.type)} ${a.name.toLowerCase()};\n`).join('')}
            ${has_phase ? 'Phase phase;\n' : undefined}
            ${generateRelations(entity.relations)}
            ${generateRoles(entity.subroles)}
            @Builder.Default
            LocalDateTime createdAt = LocalDateTime.now();

            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || this.getClass() != o.getClass()) return false;

                ${entity.name} elem = (${entity.name}) o;
                return getId().equals(elem.getId());
            }

            @Override
            public int hashCode() {
                return Objects.hash(getId());
            }
        }
    `
}

function generatePhases(entity: Readonly<Entity>) : Generated {
    return expandToStringWithNL`
        public enum Phase {
            ${entity.phases.map(x => x.toUpperCase()).join(', ')};

            public static Optional<Phase> get(String s) {
                try {
                    return Optional.of(Phase.valueOf(s.toUpperCase()));
                } catch (Exception e) {
                    return Optional.empty();
                }
            }
        }
    `
}

function generateRoles(roles: readonly Role[]) : Generated {
    const role_to_attrs = (r: Readonly<Role>): Generated[] => {
        const attrs: Generated[] = role_attributes(r).map(a =>
            `${capitalizeString(a.type)} ${a.name.toLowerCase()};\n`
        )
        const rels: Generated[] = r.relations.map(generateRelation)

        return attrs.concat(rels, ['\n'])
    }

    return roles.length > 0
        ? new CompositeGeneratorNode(...roles.flatMap(role_to_attrs))
        : undefined
}

function generateRelations(relations: readonly Relation[]) : Generated {
    return relations.length > 0
        ? new CompositeGeneratorNode(...relations.map(generateRelation))
        : undefined
}

function generateRelation({name, target, cardinality, owner} : Readonly<Relation>) : Generated {
    const tgt_name = isEntity(target) ? target.name : target.role_of?.name
    const lower_name = name.toLowerCase()
    switch(cardinality) {
        case "OneToOne":
            if (owner) {
                return expandToStringWithNL`
                    @OneToOne
                    @JoinColumn(name="${lower_name}_id", referencedColumnName="id")
                    private ${tgt_name} ${lower_name};
                `
            } else {
                return expandToStringWithNL`
                    @OneToOne(cascade={CascadeType.ALL}, orphanRemoval=true, mappedBy="${lower_name}")
                    @Builder.Default
                    private ${tgt_name} ${lower_name} = null;
                `
            }
        case "OneToMany":
            if(owner) {
                return ''
            } else {
                return expandToStringWithNL`
                    @OneToMany(cascade={CascadeType.ALL}, orphanRemoval=true, mappedBy="${lower_name}")
                    @Builder.Default
                    private Set<${tgt_name}> ${lower_name} = new HashSet<>();
                `
            }
        case "ManyToOne":
            if(owner) {
                return expandToStringWithNL`
                    @ManyToOne
                    @JoinColumn(name="${lower_name}_id")
                    private ${tgt_name} ${lower_name};
                `
            } else {
                return ''
            }
        case "ManyToMany":
            if(owner) {
                return expandToStringWithNL`
                    @ManyToMany
                    @JoinTable(
                        name="${lower_name}",
                        joinColumns=@JoinColumn(name="${lower_name}_left"),
                        inverseJoinColumns=@JoinColumn(name="${lower_name}_right")
                    )
                    @Builder.Default
                    private Set<${tgt_name}> ${lower_name} = new HashSet<>();
                `
            } else {
                return expandToStringWithNL`
                    @ManyToMany(mappedBy="${lower_name}")
                    @Builder.Default
                    private Set<${tgt_name}> ${lower_name} = new HashSet<>();
                `
            }
    }
}
