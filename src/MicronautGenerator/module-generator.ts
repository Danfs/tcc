import path from "path";
import fs from "fs";

import type { Entity, Package } from "../generator-types";
import { expandToStringWithNL, Generated, toString } from "langium";

import { createPath } from "./generator-utils";

import { generateModel } from "./module/model-generator";
import { generateController } from "./module/controller-generator";
import { generateApplication } from "./module/application-generator";
import { generateInputDTO, generateOutputDTO } from "./module/dtos-generator";
import {
    generateBaseHandler,
    generateNotFoundException,
    generateClassNotFoundException,
    generatePhaseNotFoundException,
    generateNoRoleRelationException,
    generateMissingRoleValueException,
    generateInsufficientCardinalityException
} from "./module/exception-generator";

export function generateModules(packages: Readonly<Package>, target_folder: string) : void {
    for(const entity of packages.entities) {
        const MODULE_PATH       = createPath(target_folder, "src/main/java/", entity.package.replace('.', '/'))
        const APPLICATIONS_PATH = createPath(MODULE_PATH, 'applications')
        const REPOSITORIES_PATH = createPath(MODULE_PATH, 'repositories')
        const CONTROLLERS_PATH  = createPath(MODULE_PATH, 'controllers')
        const EXCEPTIONS_PATH   = createPath(MODULE_PATH, 'exceptions')
        const MODELS_PATH       = createPath(MODULE_PATH, 'models')
        const DTOS_PATH         = createPath(MODULE_PATH, 'dtos')

        const { name } = entity
        fs.writeFileSync(path.join(MODELS_PATH,       `${name}.java`),                          toString(generateModel(entity)))
        fs.writeFileSync(path.join(DTOS_PATH,         `${name}InputDto.java`),                  toString(generateInputDTO(entity)))
        fs.writeFileSync(path.join(DTOS_PATH,         `${name}OutputDto.java`),                 toString(generateOutputDTO(entity)))
        fs.writeFileSync(path.join(CONTROLLERS_PATH,  `${name}Controller.java`),                toString(generateController(entity)))
        fs.writeFileSync(path.join(APPLICATIONS_PATH, `${name}Application.java`),               toString(generateApplication(entity)))
        fs.writeFileSync(path.join(REPOSITORIES_PATH, `${name}Repository.java`),                toString(generateClassRepository(entity)))
        fs.writeFileSync(path.join(EXCEPTIONS_PATH,   'BaseHandler.java'),                      toString(generateBaseHandler(entity.package)))
        fs.writeFileSync(path.join(EXCEPTIONS_PATH,   'BaseException.java'),                    toString(generateNotFoundException(entity.package)))
        fs.writeFileSync(path.join(EXCEPTIONS_PATH,   `${name}NotFoundException.java`),         toString(generateClassNotFoundException(entity)))
        fs.writeFileSync(path.join(EXCEPTIONS_PATH,   'RelationMissingRoleException.java'),     toString(generateNoRoleRelationException(entity)))
        fs.writeFileSync(path.join(EXCEPTIONS_PATH,   'InsufficientCardinalityException.java'), toString(generateInsufficientCardinalityException(entity)))
        for(const role of entity.subroles) {
            fs.writeFileSync(path.join(EXCEPTIONS_PATH, `${name}${role.name}MissingValueException.java`), toString(generateMissingRoleValueException(entity, role)))
        }
        if(entity.phases.length > 0) {
            fs.writeFileSync(path.join(EXCEPTIONS_PATH, `${name}PhaseNotFoundException.java`), toString(generatePhaseNotFoundException(entity)))
        }
    }
}

function generateClassRepository(entity: Readonly<Entity>) : Generated {
    return expandToStringWithNL`
        package ${entity.package}.repositories;

        import ${entity.package}.models.${entity.name};

        import java.util.UUID;
        import io.micronaut.data.annotation.Repository;
        import io.micronaut.data.jpa.repository.JpaRepository;

        @Repository
        public interface ${entity.name}Repository extends JpaRepository<${entity.name}, UUID>{

        }
    `
}
