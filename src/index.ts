import path from 'path'
import type { Package } from './generator-types'
import { generate as generateMicronaut } from './MicronautGenerator/main-generator'
import { readOntouml } from './ontouml-reader'

/**
 * When adding a new source language, you need to:
 * - Add a string identifier for the language on the `source_langs` array
 *   - This is the identifier that will identify it through the code
 * - Create a function that receives a filename of a file of your language and returns an array of Packages
 * - Add the previously mentioned function to the `readers` object, on the `readFile` function, using the language identifier as key
 */
const source_langs = [
    'ontouml-js'
] as const
type SourceLang = typeof source_langs[number]
function isSourceLang(str: string) : str is SourceLang {
    return source_langs.includes(str as SourceLang)
}

function readFile(filename: string, source_lang: SourceLang) : Package {
    const readers: {[K in SourceLang]: (s: string) => Package} = {
        'ontouml-js': readOntouml
    }
    return readers[source_lang](filename)
}

/**
 * When adding a new target language/framework, you need to: 
 * - Add a string identifier for the language/framework on the `target_langs` array
 *   - This is the identifier that will identify it through the code
 * - Create a function that receives an array of Packages and a target path string, and generates the files inside the target path, returning nothing
 * - Add the previously mentioned function to the `generators` object, on the `generate` function, using the language identifier as key
 */
const target_langs = [
    'micronaut'
] as const
type TargetLang = typeof target_langs[number]
function isTargetLang(str: string) : str is TargetLang {
    return target_langs.includes(str as TargetLang)
}

function generate(packages: Package, target_lang: TargetLang, target_dir: string) : void {
    const generators: {[K in TargetLang]: (p: Package, t: string) => void} = {
        'micronaut': generateMicronaut
    }
    const target_path = path.join(target_dir, 'generated')

    generators[target_lang](packages, target_path)
}

/**
 * Receives a file-path and the identifiers of the source and target languages, and generate the code.
 * If any of the language identifiers are unknown, an error message is printed, listing valid id options
 * @param filename Source file path
 * @param source_lang String id identifing the Language of the source file
 * @param target_lang String id identifing the language/framework of the code to be generated
 * @returns a boolean, identifing if the code was generated
 */
export function generateCode(filename: string, source_lang: string, target_lang: string) : boolean {
    if(isSourceLang(source_lang) && isTargetLang(target_lang)) {
        const packages = readFile(filename, source_lang)

        generate(packages, target_lang, path.dirname(filename))
        return true
    } else {
        console.error(`Invalid language option.\nValid source options are: ${source_langs}.\nValid target options are: ${target_langs}.`)
        return false
    }
}
