export type Cardinality = 'OneToMany' | 'OneToOne' | 'ManyToOne' | 'ManyToMany'

export type Package = {
    entities: Entity[]
}

export type Attribute = {
    name: string
    type: string
}

export type Category = {
    name: string
    attributes: Attribute[]
    // relations: Relation[]
    phases: string[]
    // subroles: Role[]
    // extends: Category[]
}

export type Entity = {
    name: string
    package: string
    attributes: Attribute[]
    relations: Relation[]
    phases: string[]
    subroles: Role[]
    categories: Category[]
    is_supertype: boolean
    extends?: Entity
    extended_attributes: Attribute[]
    extended_relations: Relation[]
}

export type Role = {
    name: string
    attributes: Attribute[]
    relations: Relation[]
    role_of?: Entity
}

export type Relation = {
    name: string
    target: Entity | Role
    cardinality: Cardinality
    owner: boolean
    minimum: number
}

// Registering Relations
export function revert_card(card: Cardinality) : Cardinality {
    switch(card) {
        case 'OneToMany':
            return 'ManyToOne'
        case 'ManyToOne':
            return 'OneToMany'
        default:
            return card
    }
}

export function card_to_string(lft_card: string | number, rgt_card: string | number) : Cardinality {
    const lft = lft_card === '1' || lft_card === 1 ?
        'One' :
        'Many'
    const rgt = rgt_card === '1' || rgt_card === 1 ?
        'One' :
        'Many'
    return `${lft}To${rgt}`
}

export function add_relation(name: string, owner: Entity | Role, non_owner: Entity | Role, card_name: Cardinality, tgt_minimum: number) : void {
    owner.relations.push({
        name: name,
        target: non_owner,
        cardinality: card_name,
        owner: true,
        minimum: tgt_minimum
    })
    non_owner.relations.push({
        name: name,
        target: owner,
        cardinality: revert_card(card_name),
        owner: false,
        minimum: 0
    })
}

// Type Guards

type GeneratorType = Package | Attribute | Category | Entity | Role | Relation

export function isEntity(elem: GeneratorType) : elem is Entity {
    return (elem as Entity).is_supertype !== undefined
}