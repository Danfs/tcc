import { OntoumlElement, Class, Relation, Generalization } from "ontouml-js"

export function isClass(elem: OntoumlElement) : elem is Class {
    return elem.type === 'Class'
}

export function isRelation(elem: OntoumlElement) : elem is Relation {
    return elem.type === 'Relation'
}

export function isGeneralization(elem: OntoumlElement) : elem is Generalization {
    return elem.type === 'Generalization'
}
