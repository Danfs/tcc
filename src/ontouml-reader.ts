import { readFileSync } from 'fs'
import {
    serializationUtils,
    Class as OntoumlClass,
    Property as OntoumlProperty,
    Relation as OntoumlRelation,
    Generalization as OntoumlGeneralization
} from 'ontouml-js'
import { isClass, isGeneralization, isRelation } from './ontouml-types'

import { add_relation, card_to_string } from "./generator-types";
import type{
    Attribute as GenAttribute,
    Category as GenCategory,
    Package as GenPackage,
    Entity as GenEntity,
    Role as GenRole,
} from './generator-types'

export function readOntouml(filename: string) : GenPackage {
    const ontouml_text     = readFileSync(filename).toString()
    const ontouml_root     = serializationUtils.parse(ontouml_text)
    const ontouml_contents = ontouml_root.getAllContents()

    const ontouml_classes         = ontouml_contents.filter(isClass)
    const ontouml_relations       = ontouml_contents.filter(isRelation)
    const ontouml_generalizations = ontouml_contents.filter(isGeneralization)

    const {entities, roles} = processClasses(ontouml_classes, ontouml_generalizations)
    processRelations(entities, roles, ontouml_relations)
    fill_extended_fields(entities)

    return {
        entities: Array.from(entities.values())
    }
}

/**
 * Creates GenEntities from a list of OntoumlClasses, processing all the generalizations (subkinds, phases, roles and categories)
 * 
 * @param classes The list of all Ontouml-js Classes
 * @param generalizations The list of all Ontouml-js generalizations
 * @returns The mapping from class_id to the Entity generated
 */
function processClasses(classes: readonly OntoumlClass[], generalizations: readonly OntoumlGeneralization[]) : {entities: Map<string, GenEntity>, roles: Map<string, GenRole>} {
    const categories = readCategories(classes)
    const entities   = readKinds(classes)
    const phases     = readPhases(classes)
    const roles      = readRoles(classes)

    processGeneralizations(generalizations, categories, entities, phases, roles)
    return {entities, roles}
}

/**
 * @param elements A list of Ontouml-js Classes, from where the datatype classes will be extracted
 * @returns A mapping of the datatype_id to the datatype_name
 */
// function readDatatypes(elements: readonly OntoumlClass[]) : Map<string, string> {
//     const datatypes = elements.filter(cls => cls.stereotype === "datatype")
//     const id_and_name: [string, string][] = datatypes.map(dt => [dt.id, dt.name.getText()])

//     return new Map(id_and_name)
// }

/**
 * @param elements A list of Ontouml-js Classes, from where the phases will be extracted
 * @returns A mapping of the class_id to the phase_name
 */
function readPhases(elements: readonly OntoumlClass[]) : Map<string, string> {
    const phases = elements.filter(cls => cls.stereotype === "phase")
    const id_and_name: [string, string][] = phases.map(ph => [ph.id, ph.name.getText()])

    return new Map(id_and_name)
}

/**
 * @param elements A list of Ontouml-js Classes, from where the Categories will be extracted
 * @returns A mapping of the class_id to the Entity generated
 */
function readCategories(elements: readonly OntoumlClass[]) : Map<string, GenCategory> {
    const categories = elements.filter(cls => cls.stereotype === 'category')
    const id_and_category: [string, GenCategory][] = categories.map(c => [c.id, OntoumlClassToGenCategory(c)])

    return new Map(id_and_category)
}

/**
 * @param elements A list of Ontouml-js Classes, from where the Kinds and Subkinds will be extracted
 * @returns A mapping of the class_id to the Entity generated
 */
function readKinds(elements: readonly OntoumlClass[]) : Map<string, GenEntity> {
    const kinds = elements.filter(cls => ['kind', 'subkind'].includes(cls.stereotype))
    const id_and_entity: [string, GenEntity][] = kinds.map(k => [k.id, OntoumlClassToGenEntity(k)])

    return new Map(id_and_entity)
}

/**
 * @param elements A list of Ontouml-js Classes, from where the Roles will be extracted
 * @returns A mapping of the class_id to the Role generated
 */
function readRoles(elements: readonly OntoumlClass[]) : Map<string, GenRole> {
    const roles = elements.filter(cls => cls.stereotype === 'role')
    const id_and_role: [string, GenRole][] = roles.map(r => [r.id, OntoumlClassToGenRole(r)])

    return new Map(id_and_role)
}

/**
 * @param entities The mapping of class_id to Entity
 * @param relations The list of all Ontouml-js Relations
 * @returns The mapping `entities` passed as argument
 */
function processRelations(entities: Map<string, GenEntity>, roles: Map<string, GenRole>, relations: readonly OntoumlRelation[]) : Map<string, GenEntity> {
    for(const rel of relations) {
        const lft_side = rel.getSourceEnd()
        const rgt_side = rel.getTargetEnd()

        const {lowerBound: lft_min, upperBound: lft_max} = lft_side.cardinality.getCardinalityBoundsAsNumbers()
        const {lowerBound: rgt_min, upperBound: rgt_max} = rgt_side.cardinality.getCardinalityBoundsAsNumbers()

        const lft_cls = entities.get(lft_side.propertyType.id) ?? roles.get(lft_side.propertyType.id)
        const rgt_cls = entities.get(rgt_side.propertyType.id) ?? roles.get(rgt_side.propertyType.id)

        if(lft_cls && rgt_cls) {
            const rel_name = `${lft_cls.name}_${rel.name.getText().replace(' ', '_')}_${rgt_cls.name}`
            const card_name = card_to_string(lft_max, rgt_max)
            if(card_name === "OneToMany") {
                add_relation(rel_name, rgt_cls, lft_cls, "ManyToOne", lft_min)
            } else {
                add_relation(rel_name, lft_cls, rgt_cls, card_name, rgt_min)
            }
        }
    }
    return entities
}

/**
 * Adds Generalizations to every Entity that has it. This can be:
 * 
 * - Setting the `extends` property, if it has supertype;
 *    - Entities that are supertypes are set with `is_supertype = true`.
 * - Adding a phase to the `phases` list, if it has phases;
 * - Adding a role to the `roles` list, if it has roles;
 * 
 * @param generalizations The list of all Ontouml-js Generalizations
 * @param categories The mapping of class_id to Category
 * @param entities The mapping of class_id to Entity
 * @param phases The mapping of class_id to phase_name
 * @param roles The mapping of class_id to Role
 * @returns The mapping `entities` passed as argument
 */
function processGeneralizations(
    generalizations: readonly OntoumlGeneralization[],
    categories: Map<string, GenCategory>,
    entities: Map<string, GenEntity>,
    phases: Map<string, string>,
    roles: Map<string, GenRole>
) : Map<string, GenEntity> {
    for(const x of generalizations) {
        // Getting from both maps, only one should return something other than undefined
        const general_entity   = entities.get(x.general.id)
        const general_category = categories.get(x.general.id)

        // If the general part is an Entity
        if(general_entity) {
            switch(x.specific.stereotype) {
                case 'subkind': {
                    const subkind = entities.get(x.specific.id)
                    if(subkind) {
                        general_entity.is_supertype = true
                        subkind.extends = general_entity
                    }
                    break
                }
                case 'phase': {
                    const phase = phases.get(x.specific.id)
                    if(phase) {
                        general_entity.phases.push(phase)
                    }
                    break
                }
                case 'role': {
                    const role = roles.get(x.specific.id)
                    if(role) {
                        general_entity.subroles.push(role)
                        role.role_of = general_entity
                    }
                    break
                }
                default:
                    break
            }
        }
        // If the general part is an Category
        else if (general_category) {
            switch(x.specific.stereotype) {
                case 'kind':
                case 'subkind': {
                    const entity = entities.get(x.specific.id)
                    if(entity) {
                        entity.categories.push(general_category)
                    }
                    break
                }
            }
        }
    }

    return entities
}

/**
 * Fills the `extended_attributes` and `extended_relations` fields of the entities with its attributes/relations and of all its supertypes.
 * @param entities The mapping of class_id to Entity
 * @returns The mapping `entities` passed as argument
 */
function fill_extended_fields(entities: Map<string, GenEntity>) : Map<string, GenEntity> {
    // First, process only the categories (category fields are treated as normal entity fields)
    // This step is know as flattening, and it is part of the required transformations
    for(const e of entities.values()) {
        e.attributes = e.categories.flatMap(i => i.attributes).concat(e.attributes)
        // e.relations  = e.categories.flatMap(i => i.relations).concat(e.relations)
    }

    // After that, preprocess extension, gobbling together the attrs and relations from its whole heredity
    // This step is just preprocessing stuff to help code generation
    for(const entity of entities.values()) {
        let acc_attr = entity.attributes
        let acc_rel  = entity.relations
        let parent   = entity.extends
        while(parent !== undefined) {
            acc_attr = parent.attributes.concat(acc_attr)
            acc_rel  = parent.relations.concat(acc_rel)
            parent   = parent.extends
        }
        entity.extended_attributes = acc_attr
        entity.extended_relations = acc_rel
    }
    return entities
}

/**
 * Converts an Ontouml-js Class with stereotype 'kind' or 'subkind' to a Generator Entity
 * @param cls The 'kind'/'subkind' class to be converted
 * @returns The generated Entity, without Relations or Supertype
 */
function OntoumlClassToGenEntity(cls: Readonly<OntoumlClass>) : GenEntity {
    return {
        name: cls.name.getText(),
        package: `base.${cls.container.name.getText()}`,
        attributes: Array.from(cls.properties, p => OntoumlPropertyToGenAttribute(p)),
        relations: [],
        phases: [],
        subroles: [],
        categories: [],
        is_supertype: false,
        extended_attributes: [],
        extended_relations: []
    }
}

/**
 * Converts an Ontouml-js Class with stereotype 'category' to a Generator Category
 * @param cls The 'category' class to be converted
 * @returns The generated Category, without Relations
 */
function OntoumlClassToGenCategory(cls: Readonly<OntoumlClass>) : GenCategory {
    return {
        name: cls.name.getText(),
        attributes: Array.from(cls.properties, p => OntoumlPropertyToGenAttribute(p)),
        // relations: [],
        phases: [],
        // subroles: [],
        // extends: []
    }
}

/**
 * Converts an Ontouml-js Class with stereotype 'role' to a Generator Role
 * @param cls The 'role' class to be converted
 * @returns The generated Entity, without Relations or Supertype
 */
function OntoumlClassToGenRole(cls: Readonly<OntoumlClass>) : GenRole {
    return {
        name: cls.name.getText(),
        attributes: cls.properties.map(p => OntoumlPropertyToGenAttribute(p)),
        relations: []
    }
}

/**
 * Converts an Ontouml-js Property to a Generator Attribute.
 * @param prop The Property to be converted
 * @returns The generated Attribute
 */
function OntoumlPropertyToGenAttribute(prop: Readonly<OntoumlProperty>) : GenAttribute {
    return {
        name: prop.name.getText(),
        type: convertType(prop.propertyType.getName())
    }
}

function convertType(type: string) : string {
    return {
        'boolean': 'Boolean',
        'string': 'String',
        'number': 'Float',
        'integer': 'Integer'
    }[type] ?? type
}
