// import path from 'path'
import { generateCode } from "."

if(process.argv.length != 5) {
    console.error("Unexpected number of arguments. This script expects 3 arguments (in order): the source language; the target language; and the source file")
    process.exit(1)
}

const [, , source_lang, target_lang, filename] = process.argv
const generated = generateCode(filename, source_lang, target_lang)

console.log(generated
    ? "Code successfully generated"
    : "Error generating code"
)
